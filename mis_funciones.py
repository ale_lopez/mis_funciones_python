def lee_entero():
    """ Solicita un valor entero y lo devuelve.
        Mientras el valor ingresado no sea entero, vuelve a solicitarlo. """
    while True:
        valor = input("Ingrese un número entero: ")
        try:
            valor = int(valor)
            return valor
        except ValueError:
            print ("ATENCIÓN: Debe ingresar un número entero.")
