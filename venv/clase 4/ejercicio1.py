class Rectangulo:

    def __init__(self, base, altura):
        self.base = base
        self.altura = altura

    def area(self):
        return self.base * self.altura


rectangulo = Rectangulo(5, 4)
print(f"El area del rectangulo es {rectangulo.area()}")
