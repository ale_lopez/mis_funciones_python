class Vehiculo:

    def __init__(self, nombre, ruedas, capacidad):
        self.nombre = nombre
        self.ruedas = ruedas
        self.capacidad = capacidad
        self.pasajeros = []

    def agregar_pasajero(self, nombre):
        if not self.asientos_disponibles():
            return False
        self.pasajeros.append(nombre)
        return True

    def asientos_disponibles(self):
        return self.capacidad - len(self.pasajeros)

minivan = Vehiculo("minivan",4,2)
personas = ["1","2","3","4","5","6","7"]

for unapersona in personas:
    if minivan.agregar_pasajero(unapersona):
        print(f"El pasajero {unapersona} fue subido exitosamente")
    else:
        print(f"No hay asiento suficiente para {unapersona}")

