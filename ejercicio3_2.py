def lee_entero():
    """ Solicita un valor entero y lo devuelve.
        Mientras el valor ingresado no sea entero, vuelve a solicitarlo. """
    while True:
        valor = input("Ingrese un número entero: ")
        try:
            valor = int(valor)
            return valor
        except ValueError:
            print ("ATENCIÓN: Debe ingresar un número entero.")


lista = []

for i in range (0,5):
    numero = lee_entero()
    lista.append(numero)

for n in lista:
    print (n)
