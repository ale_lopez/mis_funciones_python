import datetime

hora1=datetime.datetime.now()
hora2=hora1 + datetime.timedelta(hours=2)

print(f"La hora actual es {hora1.strftime('%H:%M:%S')}")
print(f"La hora adelantada es {hora2.strftime('%H:%M:%S')}")